#!/usr/bin/env python

import math
import cairo

TOP = 50

from collections import namedtuple

Measurement = namedtuple('Measurement', ['top', 'right', 'bottom', 'left'])

class Canvas():
    def __init__(self, output):
        self.output = output
        if self.output.endswith('.png'):
            self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 1000, 1000)
        elif self.output.endswith('.pdf'):
            self.surface = cairo.PDFSurface('test.pdf', 1000, 1000)
        self.ctx = cairo.Context(self.surface)

    def resize(self, w, h):
        if self.output.endswith('.pdf'):
            self.surface.set_size(w, h)
        else:
            self.surface.finish()
            self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)
            self.ctx = cairo.Context(self.surface)

    def clear(self):
        self.ctx.set_source_rgb(1.0, 1.0, 1.0)
        self.ctx.paint()

    def save(self):
        if self.output.endswith('.png'):
            self.surface.write_to_png(self.output)
        elif self.output.endswith('.pdf'):
            self.surface.show_page()

from enum import Enum

class Text():
    class HALIGN(Enum):
        LEFT = 0
        CENTER = 1
        RIGHT = 2
    class VALIGN(Enum):
        TOP = 0
        MIDDLE = 1
        BOTTOM = 2
    def __init__(self, text, fontsize, halign, valign):
        self.text = text
        self.fontsize = fontsize
        self.halign = halign
        self.valign = valign
        self._m = None
        self.top = None
        self.right = None
        self.bottom = None
        self.left = None
        self.lineheight = None
        self.baseline = None

    def _confctx(self, ctx):
        ctx.select_font_face("Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        ctx.set_font_size(self.fontsize)
        ctx.set_source_rgb(0.0, 0.0, 0.0)

    def _meas(self, ctx):
        self._confctx(ctx)
        if self.lineheight is None:
            (axb, ayb, awidth, aheight, adx, ady) = ctx.text_extents('Ág')
            self.lineheight = aheight
            self.baseline = ayb
        if self._m is None:
            self._m = ctx.text_extents(self.text)
        return self._m

    def measure(self, ctx):
        (xb, yb, width, height, dx, dy) = self._meas(ctx)

        if self.halign == self.HALIGN.LEFT:
            self.left = 0
            self.right = width
        elif self.halign == self.HALIGN.CENTER:
            self.left = width / 2
            self.right = width / 2
        elif self.halign == self.HALIGN.RIGHT:
            self.left = width
            self.right = 0

        if self.valign == self.VALIGN.TOP:
            self.top = 0
            self.bottom = self.lineheight
        elif self.valign == self.VALIGN.MIDDLE:
            self.top = self.lineheight / 2
            self.bottom = self.lineheight / 2
        elif self.valign == self.VALIGN.BOTTOM:
            self.top = self.lineheight
            self.bottom = 0

    def draw(self, ctx, x, y):
        (xb, yb, width, height, dx, dy) = self._meas(ctx)

        if self.halign == self.HALIGN.CENTER:
            x -= xb + width / 2
        elif self.halign == self.HALIGN.RIGHT:
            x -= dx

        if self.valign == self.VALIGN.TOP:
            y -= self.baseline
        elif self.valign == self.VALIGN.MIDDLE:
            y -= self.baseline + self.lineheight / 2
        elif self.valign == self.VALIGN.BOTTOM:
            y -= self.baseline + self.lineheight

        ctx.move_to(x, y)
        ctx.show_text(self.text)


from functools import reduce

class MultilineText():
    def __init__(self, text, fontsize, halign, valign):
        self.valign = valign
        self.ts = []
        for s in text.split('\n'):
            self.ts.append(Text(s, fontsize, halign, Text.VALIGN.TOP))
        self.top = None
        self.right = None
        self.bottom = None
        self.left = None

    def measure(self, ctx):
        for t in self.ts:
            t.measure(ctx)
        height = reduce(lambda a, e: a + e.lineheight, self.ts, 0)
        self.left = max(map(lambda t: t.left, self.ts))
        self.right = max(map(lambda t: t.right, self.ts))

        if self.valign == Text.VALIGN.TOP:
            self.top = 0
            self.bottom = height
        elif self.valign == Text.VALIGN.MIDDLE:
            self.top = height / 2
            self.bottom = height / 2
        elif self.valign == Text.VALIGN.BOTTOM:
            self.top = height
            self.bottom = 0

    def draw(self, ctx, x, y):
        if self.top is None:
            self.measure(ctx)
        cy = y - self.top
        for t in self.ts:
            t.draw(ctx, x, cy)
            cy += t.lineheight



def drawHelper(ctx, x, y, lines=False):
    ctx.set_source_rgb(1.0,0.0,0.0)
    if not lines:
        ctx.rectangle(x-1, y-1, 2, 2)
        ctx.fill()
    else:
        ctx.move_to(x, 0)
        ctx.line_to(x, 1000)
        ctx.stroke()
        ctx.move_to(0, y)
        ctx.line_to(1000, y)
        ctx.stroke()





class Participant():
    fontsize = 16
    padding = 14
    def __init__(self, name, lifeline_len):
        self.lifeline_len = lifeline_len
        self.name = name
        self.t  = MultilineText(name, self.fontsize, Text.HALIGN.CENTER, Text.VALIGN.BOTTOM)
        self.top = None
        self.right = None
        self.bottom = None
        self.left = None

    def measure(self, ctx):
        self.t.measure(ctx)
        self.top = self.t.top + self.padding
        self.right = self.t.right + self.padding
        self.bottom = self.t.bottom + self.padding
        self.left = self.t.left + self.padding

    def draw(self, ctx, x, y):
        if self.top is None:
            self.measure(ctx)
        self.t.draw(ctx, x, y)
        # Box
        ctx.set_line_width(1.5)
        ctx.set_source_rgb(0.0,0.0,0.0)
        ctx.rectangle(x - self.left, y - self.top, self.left + self.right, self.top + self.bottom)
        ctx.stroke()

        ctx.set_line_width(1.5)
        ctx.set_source_rgb(0.4,0.4,0.4)
        ctx.move_to(x, y + self.padding)
        ctx.line_to(x, y + self.padding + self.lifeline_len)
        ctx.stroke()


class Call():
    fontsize = 14
    ARROW_W = 10
    ARROW_H = 7

    class ARROWTYPE(Enum):
        NONE = 0
        FILLED = 1

    def __init__(self, text, arrowtype):
        self.t = MultilineText(text, self.fontsize, Text.HALIGN.CENTER, Text.VALIGN.BOTTOM)
        self.arrowtype = arrowtype
        self.top = None
        self.right = None
        self.bottom = None
        self.left = None

    def measure(self, ctx):
        self.t.measure(ctx)
        self.top = self.t.top
        self.right = self.t.right
        self.bottom = self.t.bottom
        self.left = self.t.left

    def draw(self, ctx, x1, x2, y):
        ctx.move_to(x1, y)
        ctx.set_line_width(1.0)
        ctx.set_source_rgb(0.0,0.0,0.0)
        ctx.line_to(x2, y)
        ctx.stroke()

        if self.arrowtype == self.ARROWTYPE.FILLED:
            if x1 < x2:
                o = self.ARROW_W
            else:
                o = -self.ARROW_W

            ctx.move_to(x2 - o, y + self.ARROW_H)
            ctx.line_to(x2, y)
            ctx.line_to(x2 - o, y - self.ARROW_H)
            ctx.close_path()
            ctx.fill()

        self.t.draw(ctx, (x1 + x2) / 2, y)





parties = []
sparties = []
calls = []

def parseCommand(cmd):
    if ':' in cmd:
        s1 = cmd.split(':')
        if '->' in s1[0]:
            s2 = s1[0].split('->')
            calls.append((s2[0], s2[1], '->', s1[1].replace('\\n', '\n').strip()))
    else:
        s1 = cmd.split(' ')
        parties.append(s1[1].replace('\\n', '\n'))
        sparties.append(s1[0])

def parse(code):
    lines = code.splitlines()
    for l in lines:
        l = l.strip()
        if len(l) != 0:
            parseCommand(l)


import sys
with open(sys.argv[1]) as f:
    parse(f.read())

# print(parties)
# print(calls)

cvs = Canvas(sys.argv[2])
cvs.clear()

CANVAS_PADDING = 30
PART_SPACING = 40
CALL_SPACING = 20
CALL_HMARGIN = 10

cs = []
for c in calls:
    c = Call(c[3], Call.ARROWTYPE.FILLED)
    c.measure(cvs.ctx)
    cs.append(c)

calls_len = reduce(lambda a, e: a + e.top + e.bottom + CALL_SPACING, cs, 0)
calls_len += CALL_SPACING

ps = []
for p in parties:
    p = Participant(p, calls_len)
    p.measure(cvs.ctx)
    ps.append(p)

top = 0
for p in ps:
    if p.top > top:
        top = p.top

HEIGHT = CANVAS_PADDING*2 + top + ps[0].bottom + calls_len

# Bind
for i, c in enumerate(calls):
    s = ps[sparties.index(c[0])]
    t = ps[sparties.index(c[1])]
    cs[i].source = s
    cs[i].target = t



xs = []

x = ps[0].left + CANVAS_PADDING
xs.append(x)

for i in range(0, len(ps)-1):
    maxwidht = ps[i].right + ps[i+1].left + PART_SPACING
    for c in cs:
        if c.source == ps[i] and c.target == ps[i+1] or c.target == ps[i] and c.source == ps[i+1]:
            if c.left + c.right + CALL_HMARGIN*2 > maxwidht:
                maxwidht = c.left + c.right + CALL_HMARGIN*2
    xs.append(maxwidht)


WIDTH = reduce(lambda a, e: a + e, xs, 0) + ps[-1].right + CANVAS_PADDING

cvs.resize(WIDTH, HEIGHT)


# DRAW

x = 0
for i, p in enumerate(ps):
    x += xs[i]
    p.x = x
    p.draw(cvs.ctx, x, CANVAS_PADDING + top)


y = CANVAS_PADDING + top + ps[0].bottom
for c in cs:
    y += c.top + c.bottom + CALL_SPACING
    c.draw(cvs.ctx, c.source.x, c.target.x, y)

cvs.save()
